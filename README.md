## Link: https://drawing-app-jscanvas.netlify.app/

### Additional features:

- 1. [DONE] Add buttons for various shapes - square
- 2. [DONE] Add "Save Image" button, to save the drawing
- 3. [DONE] Add eraser button with different sizes
- 4. Add different line styles - different brushes, dotted line, dashed line
- 5. Fill closed figures with color
- 6. [DONE] Adda ability to create dots with a single mouse button click
- 7. Add undo / redo actions
